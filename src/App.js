import './assets/css/style.css';

// assets image
import banner from './assets/img/architect.jpg';
import house2 from './assets/img/house2.jpg';
import house3 from './assets/img/house3.jpg';
import house4 from './assets/img/house4.jpg';
import house5 from './assets/img/house5.jpg';
import map from './assets/img/map.jpg';
import team1 from './assets/img/team1.jpg';
import team2 from './assets/img/team2.jpg';
import team3 from './assets/img/team3.jpg';
import team4 from './assets/img/team4.jpg';

function App() {
  return (
    <div className="App">

      {/* Navbar (sit on top) */}
      <div className="navbar">
        <div className="left"> <a href="/#"><b>E D T S </b> &nbsp;T D P &nbsp;B a t c h &nbsp;# 2</a> </div>
        <div className="right">
          <a href="/#projects">P r o j e c t s &nbsp;</a>
          <a href="/#about">A b o u t &nbsp;</a>
          <a href="/#contact">C o n t a c t</a>
        </div>
      </div>

      {/* Header */}
      <header>
        <img src={banner} alt="Architecture" />
        <div className="mid">
          <h1 className="black-border">
            <b>EDTS</b> <span>Architects</span>
          </h1>
        </div>
      </header>

      {/* Page content */}
      <div className="container">

        {/* Project Section */}
        <div id="projects">
          <h3>Projects</h3>
        </div>
        <hr></hr>

        <div className="row">
          <div className="gallery">
            <div className="text">Summer House</div>
            <img src={house2} alt="House" />
          </div>
          <div className="gallery">
            <div className="text">Brick House</div>
            <img src={house3} alt="House" />
          </div>
          <div className="gallery">
            <div className="text">Renovated</div>
            <img src={house4} alt="House" />
          </div>
          <div className="gallery">
            <div className="text">Barn House</div>
            <img src={house5} alt="House" />
          </div>
          <div className="gallery">
            <div className="text">Summer House</div>
            <img src={house3} alt="House" />
          </div>
          <div className="gallery">
            <div className="text">Brick House</div>
            <img src={house2} alt="House" />
          </div>
          <div className="gallery">
            <div className="text">Renovated</div>
            <img src={house5} alt="House" />
          </div>
          <div className="gallery">
            <div className="text">Barn House</div>
            <img src={house4} alt="House" />
          </div>
          
        </div>
        {/* About Section */}
        <div id="about">
          <h3 className="about">About</h3>
          <hr></hr>
          <p className="p-about">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Excepteur sint
            occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.
          </p>
        </div>

        <div className="row-about">
          <div className="gallery-about">
            <img src={team1} alt="Jane" />
            <h3 >Jane Doe</h3>
            <p>CEO &amp; Founder</p>
            <p className="p-about">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p className="p-button"><button className="button-m">Contact</button></p>
          </div>
          <div className="gallery-about">
            <img src={team2} alt="John" />
            <h3 >John Doe</h3>
            <p>Architect</p>
            <p className="p-about">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p className="p-button"><button className="button-m">Contact</button></p>
          </div>
          <div className="gallery-about">
            <img src={team3} alt="Mike" />
            <h3 >Mike Ross</h3>
            <p>Architect</p>
            <p className="p-about">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p className="p-button"><button className="button-m">Contact</button></p>
          </div>
          <div className="gallery-about">
            <img src={team4} alt="Dan" />
            <h3 >Dan Star</h3>
            <p>Architect</p>
            <p className="p-about">Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <p className="p-button"><button className="button-m">Contact</button></p>
          </div>
        </div>

        {/* Contact Section */}
        <div id="contact">
          <h3 className="contact">Contact</h3>
          <hr></hr>
          <p className="p-contact">Lets get in touch and talk about your next project.</p>

          {/*** Form ***/}

          <div className="wrapper">
            <form id="formsubmit" action="/post">
              <div class="fields">
                <input className="input-fields" placeholder="Name" ></input>
              </div>
              <div class="fields">
                <input id="email" class="input-fields" placeholder="Email" ></input>
              </div>
              <div class="fields">
                <input id="subject" class="input-fields" placeholder="Subject" ></input>
              </div>
              <div class="fields">
                <input id="comment" class="input-fields" placeholder="Comment" ></input>
              </div>
              <button class="button" id="submit" type="submit">Send Message</button>
            </form>
          </div>
        </div>

        {/* Image of location/map */}
        <div className="maps">
          <img src={map} alt="maps" />
        </div>

        <div>
          <h3>Contact List</h3>
          <hr></hr>

          <table>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Country</th>
            </tr>
            <tr>
              <td>Alfreds Futterkiste</td>
              <td>alfreds@gmail.com</td>
              <td>Germany</td>
            </tr>
            <tr>
              <td >Reza</td>
              <td >reza@gmail.com</td>
              <td >Indonesia</td>
            </tr>
            <tr>
              <td>Ismail</td>
              <td>ismail@gmail.com</td>
              <td>Turkey</td>
            </tr>
            <tr>
              <td >Holand</td>
              <td >holand@gmail.com</td>
              <td >Belanda</td>
            </tr>
          </table>

        </div>

        {/* End page content */}
      </div>

      {/* Footer */}
      <footer>
        <p>Copyright 2022</p>
      </footer>

    </div>
  );
}

export default App;
